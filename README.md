# SHRIKE.ME | [![Travis](https://img.shields.io/travis/SShrike/website.svg)](https://travis-ci.org/SShrike/website) [![Greenkeeper](https://badges.greenkeeper.io/SShrike/website.svg)](https://greenkeeper.io/)

This is the source code for my [personal website](https://shrike.me/).
