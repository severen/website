import * as React from 'react';

const ThanksPage = () => (
  <div>
    <h1>Thanks!</h1>
    <p>Your message should now be sitting in my inbox.</p>
  </div>
);

export default ThanksPage;
