import * as React from 'react';

const NotFoundPage = () => (
  <div>
    <h1>Page Not Found</h1>
    <p>
      The page you requested does not exist. Perhaps you have made a typo, or it
      has otherwise been deleted or moved.
    </p>
  </div>
);

export default NotFoundPage;
